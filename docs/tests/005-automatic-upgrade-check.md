# Automatic Upgrade Check

## Intent

Unless the setting is disabled, the monitor should automically check for newer versions of itself as well as monitored Tezos nodes and tell the user.

## Tests for Kiln Upgrade Check

Note that the command line options can also be set via the `config/upgrade-branch` and `config/check-for-upgrade` files.  However, developers should note that this feature doesn't fully work with `ob run` because in that context Kiln doesn't know what version it is, and simply assumes it's running version 0.

### Testing when there is a new Kiln version
  1. Run the monitor like this: `./backend --upgrade-branch=TEST-UPGRADE`
  2. Open the monitor in the browser and you should immediately see a banner saying that there is a new version.
  3. In the Options tab, click "Check for New Version".
  4. It should tell you there is a new version.

### Testing when no new Kiln version
  1. Run the monitor like this: `./backend --upgrade-branch=develop`
  2. Open the monitor in the browser and there should be not be a banner mentioning upgrades.
  3. In the Options tab, click "Check for New Version".
  4. It should tell you that you're using the latest version.

### Testing when the Kiln version check fails
  1. Run the monitor like this: `./backend --upgrade-branch=not-a-real-branch`.
  2. Open the monitor in the browser and you should immediately see a banner saying that the version check failed.
  3. In the Options tab, click "Check for New Version".
  4. It should tell you that the check failed.

### Testing that the upgrade check can be disabled
  1. Run the monitor like this: `./backend --check-for-upgrade=no`.
  2. Open the monitor in the browser and there should be not be a banner mentioning upgrades.
  3. In the Options tab, the "Check for New Version" but should be missing.

## Tests for Tezos node upgrade check

Note that this feature only applies to external Tezos nodes:  no check is performed on the Kiln-managed node as there's no way for an end user to upgrade the Kiln node independently of Kiln itself.  In a sense, the kiln mananged node is covered by the Kiln Upgrade Check.

  1. Use mainnet.  Add an external tezos node.  Obsidian QA can to ask about an external node they can use.  Outside QA teams will need to create one themselves,  or have one created for them.

  2. Restart Kiln.   Run the monitor like this: `./backend --network-gitlab-project-id=5906749`

  3. You should see a notification that the external tezos node added in step 1 is out of date.

### Notes for developers and the curious:

  1. This feature simply compares the commit hash of the external node (obtained via the `/monitor/commit_hash` RPC) to the most recent commit of the `mainnet` branch of the given project id,  which defaults to the project id of <https://www.gitlab.com/tezos/tezos>,  and if these two commit hashes aren't equal, the notification is generated.

  2. Mainnet isn't strictly necessary:  test networks should work just as well.  However, the branch will then change: e.g for `carthagenet` then the code will look for a `carthagenet` branch.

  3. The project id provided above corresponds to <https://www.gitlab.com/obsidian.systems/tezos>,  in which the `mainnet` branch haven't been kept up to date in a while, and the `carthagenet` branch doesn't exist.  (Why do we use project id URLs instead of the corresponding more user-friendly urls?)

  4. This check is performed immediately at startup,  and then once an hour thereafter.  There currently is no way to configure the frequency of this check.  Thus the instruction to Restart Kiln in step #2.   (Should this be performed immediately when adding a new external node?)

  5. The command line option can be set via the `config/network-gitlab-project-id` file as well.

  6. This feature works perfectly well under `ob run`.

  7. Setting `check-for-upgrade` to false also disables the Tezos node check.
